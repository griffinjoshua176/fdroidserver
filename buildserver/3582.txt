diff --git a/metadata/at.weiss.matthias.musicala.yml b/metadata/at.weiss.matthias.musicala.yml
index 98ab97018393a731bc903dfb5baa8d33f8027dcb..a2d5ee461719d4525d06e5dd47b7cd3082ecada9 100644
--- a/metadata/at.weiss.matthias.musicala.yml
+++ b/metadata/at.weiss.matthias.musicala.yml
@@ -1,9 +1,8 @@
-Disabled: 'True'
 Categories:
   - Multimedia
   - Connectivity
 License: GPL-3.0-or-later
-AuthorName: Matthias Weiss and M.A.L.P team
+AuthorName: Matthias Weiss
 WebSite: https://gitlab.com/matthias-weiss/musicala/blob/HEAD/README.md
 SourceCode: https://gitlab.com/matthias-weiss/musicala
 IssueTracker: https://gitlab.com/matthias-weiss/musicala/issues
@@ -25,19 +24,19 @@ Builds:
 
   - versionName: 0.9.1
     versionCode: 2
-    commit: '2'
+    commit: 0.9.1
     subdir: app
     gradle:
-      - yes
+      - standard
 
   - versionName: 0.9.2
     versionCode: 3
-    commit: '3'
+    commit: 0.9.2
     subdir: app
     gradle:
-      - yes
+      - standard
 
-AutoUpdateMode: Version %c
+AutoUpdateMode: Version %v
 UpdateCheckMode: Tags
 CurrentVersion: 0.9.2
 CurrentVersionCode: 3
